<?php

namespace app\controllers;

use Yii;
use app\models\Categorias;
use app\models\Remeros;
use app\models\Entrenadores;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;


/**
 * CategoriasController implements the CRUD actions for Categorias model.
 */
class CategoriasController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Categorias models.
     * @return mixed
     */
    public function actionIndex()
    {
        $numremeros = Remeros::find()->count();
        $numentrenadores = Entrenadores::find()->count();
        
  
        
        $dataProvider = new ActiveDataProvider([
            'query' => Categorias::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'numremeros' => $numremeros,
            'numentrenadores' => $numentrenadores,
        ]);
    }

    /**
     * Displays a single Categorias model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Categorias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Categorias();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    
    public function actionPdf($codigo) {
    //Consulta remeros
    $remeros= new ActiveDataProvider([
           'query'=>Remeros::find()
                ->select("codigo_remero,nombre_completo,dni")
                ->where("codigo_categoria='$codigo'"),
        ]);
    
    //Consulta entrenadores    
    $entrenadores= new ActiveDataProvider([
       'query'=>Entrenadores::find()->select('nombre_completo,dni')
            ->where("codigo_categoria='$codigo'")
    ]);
    // Renderizo los datros en la vista de remeroscategoria
    $content = $this->renderPartial('remerospdf',[
            "entrenadores" => $entrenadores,
            "campos_entrenadores" => ['nombre_completo','dni'],
            "titulo_entrenadores" => "El entrenador de la categoría ".$codigo." es: ",
            "remeros"=>$remeros,
            "campos_remeros"=>['codigo_remero','nombre_completo','dni'],
            "titulo_remeros"=>"Los remeros que la componen son:",
            "codigo" => $codigo,
    ]);
    // setup kartik\mpdf\Pdf component
    $remerosporcategoria = new Pdf([
        // set to use core fonts only
        'mode' => Pdf::MODE_CORE, 
        // A4 paper format
        'format' => Pdf::FORMAT_A4, 
        // portrait orientation
        'orientation' => Pdf::ORIENT_PORTRAIT, 
        // stream to browser inline
        'destination' => Pdf::DEST_BROWSER, 
        // your html content input
        'content' => $content,  
        'cssInline' => '.kv-heading-1{font-size:18px}', 
         // set mPDF properties on the fly
        'options' => ['title' => 'Krajee Report Title'],
         // call mPDF methods on the fly
        'methods' => [ 
            'SetHeader'=>['Rowwwing APP'], 
            'SetFooter'=>['{PAGENO}'],
        ]
    ]);
    
    return $remerosporcategoria->render(); 

    }

    /**
     * Updates an existing Categorias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->codigo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Categorias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    public function actionRemerosporcategoria($codigo) {
        
        $remeros= new ActiveDataProvider([
           'query'=>Remeros::find()
                ->select("codigo_remero,nombre_completo,dni")
                ->where("codigo_categoria='$codigo'"),
        ]);
        
        
        $entrenadores= new ActiveDataProvider([
           'query'=>Entrenadores::find()->select('nombre_completo,dni')
                ->where("codigo_categoria='$codigo'")
        ]);
        
        return $this->render("remeroscategoria",[
            "entrenadores" => $entrenadores,
            "campos_entrenadores" => ['nombre_completo','dni'],
            "titulo_entrenadores" => "El entrenador de la categoría ".$codigo." es: ",
            
            "remeros"=>$remeros,
            "campos_remeros"=>['codigo_remero','nombre_completo','dni'],
            "titulo_remeros"=>"Los remeros que la componen son:",
            
            
            "codigo" => $codigo,
        ]);
        
    }

    /**
     * Finds the Categorias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Categorias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Categorias::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
