<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Remeros;
use app\models\Entrenadores;
use app\models\Patrocinadores;
use app\models\Barcos;
use app\models\Remos;
use app\models\Gastos;
use app\models\Cuotas;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        $numremeros = Remeros::find()->count();
        $numentrenadores = Entrenadores::find()->count();
        $numpatrocinadores = Patrocinadores::find()->count();
        $numbarcos = Barcos::find()->count();
        $numremos = Remos::find()->count();
        
        return $this->render('index',
                [
                    'numremeros' => $numremeros,
                    'numentrenadores' => $numentrenadores,
                    'numpatrocinadores' => $numpatrocinadores,
                    'numbarcos' => $numbarcos,
                    'numremos' => $numremos,
                ]);
    }
    
    public function actionTesoreria()
    {
        $cantgastos = Gastos::find()->sum('cantidad');
        $cantcuotas = Cuotas::find()->sum('importe');
        $cantpatrocinadores = Patrocinadores::find()->sum('cantidad_aportada');
        
 
        return $this->render('tesoreria', [
            'cantgastos' => $cantgastos,
            'cantcuotas' => $cantcuotas,
            'cantpatrocinadores' => $cantpatrocinadores,
        ]);
    } 

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
