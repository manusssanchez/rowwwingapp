<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "barcos".
 *
 * @property string $matricula
 * @property string $nombre_tecnico
 * @property string|null $mote
 * @property string|null $fabricante
 *
 * @property Patrocinios[] $patrocinios
 * @property Patrocinadores[] $codigoPatrocinadors
 * @property Usosmaterial[] $usosmaterials
 */
class Barcos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'barcos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula', 'nombre_tecnico'], 'required'],
            [['matricula'], 'string', 'max' => 6],
            [['nombre_tecnico'], 'string', 'max' => 30],
            [['mote', 'fabricante'], 'string', 'max' => 15],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'matricula' => 'Matrícula',
            'nombre_tecnico' => 'Nombre técnico',
            'mote' => 'Mote',
            'fabricante' => 'Fabricante',
        ];
    }

    /**
     * Gets query for [[Patrocinios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinios()
    {
        return $this->hasMany(Patrocinios::className(), ['matricula_barco' => 'matricula']);
    }

    /**
     * Gets query for [[CodigoPatrocinadors]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPatrocinadors()
    {
        return $this->hasMany(Patrocinadores::className(), ['codigo' => 'codigo_patrocinador'])->viaTable('patrocinios', ['matricula_barco' => 'matricula']);
    }

    /**
     * Gets query for [[Usosmaterials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsosmaterials()
    {
        return $this->hasMany(Usosmaterial::className(), ['matricula_barco' => 'matricula']);
    }
}
