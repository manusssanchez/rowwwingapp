<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cuotas".
 *
 * @property int $id_cuota
 * @property int|null $id_remero
 * @property string|null $mes
 * @property float|null $importe
 * @property string|null $fecha_pago
 *
 * @property Remeros $remero
 */
class Cuotas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cuotas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_remero', 'fecha_pago', 'mes'], 'required'],
            [['id_remero'], 'integer'],
            [['importe'], 'number'],
            [['fecha_pago'], 'safe'],
            [['mes'], 'string', 'max' => 10],
            [['id_remero'], 'exist', 'skipOnError' => true, 'targetClass' => Remeros::className(), 'targetAttribute' => ['id_remero' => 'id_remero']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cuota' => 'Id Cuota',
            'id_remero' => 'Id Remero',
            'mes' => 'Mes',
            'importe' => 'Importe',
            'fecha_pago' => 'Fecha de pago',
        ];
    }

    /**
     * Gets query for [[Remero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRemero()
    {
        return $this->hasOne(Remeros::className(), ['id_remero' => 'id_remero']);
    }
    
    
    public function afterFind() {
        parent::afterFind();
        $this->fecha_pago=Yii::$app->formatter->asDate($this->fecha_pago, 'php:d-m-Y');
    }
    
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha_pago= \DateTime::createFromFormat("d/m/Y", $this->fecha_pago)->format("Y/m/d");
        return true;
    }
    
    
}
