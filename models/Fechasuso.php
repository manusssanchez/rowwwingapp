<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fechasuso".
 *
 * @property int $id_fecha
 * @property int|null $id_uso
 * @property string|null $fecha_uso
 *
 * @property Usosmaterial $uso
 */
class Fechasuso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fechasuso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_uso'], 'integer'],
            [['fecha_uso'], 'safe'],
            [['id_uso', 'fecha_uso'], 'unique', 'targetAttribute' => ['id_uso', 'fecha_uso']],
            [['id_uso'], 'exist', 'skipOnError' => true, 'targetClass' => Usosmaterial::className(), 'targetAttribute' => ['id_uso' => 'id_uso']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_fecha' => 'ID Fecha',
            'id_uso' => 'ID Uso',
            'fecha_uso' => 'Fecha Uso',
        ];
    }

    /**
     * Gets query for [[Uso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUso()
    {
        return $this->hasOne(Usosmaterial::className(), ['id_uso' => 'id_uso']);
    }
    
    
    public function afterFind() {
        parent::afterFind();
        $this->fecha_uso=Yii::$app->formatter->asDate($this->fecha_uso, 'php:d-m-Y');
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha_uso= \DateTime::createFromFormat("d/m/Y", $this->fecha_uso)->format("Y/m/d");
        return true;
    }
    
}
