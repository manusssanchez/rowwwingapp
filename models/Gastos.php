<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gastos".
 *
 * @property int $id_gasto
 * @property string|null $asunto
 * @property float|null $cantidad
 * @property string|null $dni_directivo
 *
 * @property Directivos $dniDirectivo
 */
class Gastos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gastos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asunto','cantidad','dni_directivo'],'required'],
            [['cantidad'], 'number'],
            [['asunto'], 'string', 'max' => 20],
            [['dni_directivo'], 'string', 'max' => 9],
            [['dni_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['dni_directivo' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_gasto' => 'ID Gasto',
            'asunto' => 'Asunto',
            'cantidad' => 'Cantidad',
            'dni_directivo' => 'DNI Directivo',
        ];
    }

    /**
     * Gets query for [[DniDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['dni' => 'dni_directivo']);
    }
}
