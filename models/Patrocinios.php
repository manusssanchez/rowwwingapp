<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinios".
 *
 * @property int $id_patrocinio
 * @property int|null $codigo_patrocinador
 * @property string|null $matricula_barco
 *
 * @property Barcos $matriculaBarco
 * @property Patrocinadores $codigoPatrocinador
 */
class Patrocinios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_patrocinador'], 'integer'],
            [['matricula_barco'], 'string', 'max' => 6],
            [['codigo_patrocinador', 'matricula_barco'], 'unique', 'targetAttribute' => ['codigo_patrocinador', 'matricula_barco']],
            [['matricula_barco'], 'exist', 'skipOnError' => true, 'targetClass' => Barcos::className(), 'targetAttribute' => ['matricula_barco' => 'matricula']],
            [['codigo_patrocinador'], 'exist', 'skipOnError' => true, 'targetClass' => Patrocinadores::className(), 'targetAttribute' => ['codigo_patrocinador' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_patrocinio' => 'ID Patrocinio',
            'codigo_patrocinador' => 'Código Patrocinador',
            'matricula_barco' => 'Matrícula Barco',
        ];
    }

    /**
     * Gets query for [[MatriculaBarco]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculaBarco()
    {
        return $this->hasOne(Barcos::className(), ['matricula' => 'matricula_barco']);
    }

    /**
     * Gets query for [[CodigoPatrocinador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPatrocinador()
    {
        return $this->hasOne(Patrocinadores::className(), ['codigo' => 'codigo_patrocinador']);
    }
}
