<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "remeros".
 *
 * @property int $id_remero
 * @property string|null $codigo_categoria
 * @property int|null $codigo_remero
 * @property string $dni
 * @property string $nombre_completo
 * @property string|null $fecha_nac
 * @property int|null $codigo_patrocinador
 * @property string|null $lesiones
 * @property int|null $anios_exp
 * @property string|null $dni_padre
 * @property string|null $nombre_padre
 *
 * @property Cuotas[] $cuotas
 * @property Categorias $codigoCategoria
 * @property Patrocinadores $codigoPatrocinador
 */
class Remeros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remeros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_remero', 'codigo_patrocinador', 'anios_exp'], 'integer'],
            [['dni', 'nombre_completo','codigo_categoria'], 'required'],
            [['fecha_nac'], 'safe'],
            [['codigo_categoria'], 'string', 'max' => 6],
            [['dni'], 'string', 'max' => 9],
            ['dni', 'match', 'pattern' => '/^([0-9]{8})([A-Z])$/i'],
            ['dni_padre', 'match', 'pattern' => '/^([0-9]{8})([A-Z])$/i'],
            [['nombre_completo'], 'string', 'max' => 30],
            [['lesiones', 'dni_padre', 'nombre_padre'], 'string', 'max' => 50],
            [['dni'], 'unique'],
            [['codigo_categoria', 'codigo_remero'], 'unique', 'targetAttribute' => ['codigo_categoria', 'codigo_remero']],
            [['codigo_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['codigo_categoria' => 'codigo']],
            [['codigo_patrocinador'], 'exist', 'skipOnError' => true, 'targetClass' => Patrocinadores::className(), 'targetAttribute' => ['codigo_patrocinador' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_remero' => 'ID Remero',
            'codigo_categoria' => 'Código Categoria',
            'codigo_remero' => 'Número de licencia',
            'dni' => 'DNI',
            'nombre_completo' => 'Nombre completo',
            'fecha_nac' => 'Fecha de nacimiento',
            'codigo_patrocinador' => 'Código Patrocinador',
            'lesiones' => 'Lesiones',
            'anios_exp' => 'Años Exp',
            'dni_padre' => 'DNI Padre',
            'nombre_padre' => 'Nombre del padre',
        ];
    }

    /**
     * Gets query for [[Cuotas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCuotas()
    {
        return $this->hasMany(Cuotas::className(), ['id_remero' => 'id_remero']);
    }

    /**
     * Gets query for [[CodigoCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCategoria()
    {
        return $this->hasOne(Categorias::className(), ['codigo' => 'codigo_categoria']);
    }

    /**
     * Gets query for [[CodigoPatrocinador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPatrocinador()
    {
        return $this->hasOne(Patrocinadores::className(), ['codigo' => 'codigo_patrocinador']);
    }
    
    
    public function afterFind() {
        parent::afterFind();
        $this->fecha_nac=Yii::$app->formatter->asDate($this->fecha_nac, 'php:d/m/Y');
    }
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $this->fecha_nac= \DateTime::createFromFormat("d/m/Y", $this->fecha_nac)->format("Y/m/d");
        return true;
    }
    
    
}
