<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "remos".
 *
 * @property int $codigo
 * @property string $fabricante
 * @property int|null $num_remos
 * @property string|null $dureza
 *
 * @property Usosmaterial[] $usosmaterials
 */
class Remos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fabricante','num_remos','dureza'], 'required'],
            [['num_remos'], 'integer'],
            [['fabricante'], 'string', 'max' => 15],
            [['dureza'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Código',
            'fabricante' => 'Fabricante',
            'num_remos' => 'Número de remos',
            'dureza' => 'Dureza',
        ];
    }

    /**
     * Gets query for [[Usosmaterials]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsosmaterials()
    {
        return $this->hasMany(Usosmaterial::className(), ['codigo_remos' => 'codigo']);
    }
}
