<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usosmaterial".
 *
 * @property int $id_uso
 * @property string|null $matricula_barco
 * @property int|null $codigo_remos
 * @property string|null $codigo_categoria
 *
 * @property Fechasuso[] $fechasusos
 * @property Barcos $matriculaBarco
 * @property Categorias $codigoCategoria
 * @property Remos $codigoRemos
 */
class Usosmaterial extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usosmaterial';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_remos', 'matricula_barco', 'codigo_categoria'], 'required'],
            [['codigo_remos'], 'integer'],
            [['matricula_barco'], 'string', 'max' => 6],
            [['codigo_categoria'], 'string', 'max' => 2],
            [['matricula_barco', 'codigo_remos', 'codigo_categoria'], 'unique', 'targetAttribute' => ['matricula_barco', 'codigo_remos', 'codigo_categoria']],
            [['matricula_barco'], 'exist', 'skipOnError' => true, 'targetClass' => Barcos::className(), 'targetAttribute' => ['matricula_barco' => 'matricula']],
            [['codigo_categoria'], 'exist', 'skipOnError' => true, 'targetClass' => Categorias::className(), 'targetAttribute' => ['codigo_categoria' => 'codigo']],
            [['codigo_remos'], 'exist', 'skipOnError' => true, 'targetClass' => Remos::className(), 'targetAttribute' => ['codigo_remos' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_uso' => 'ID Uso',
            'matricula_barco' => 'Matrícula de barco',
            'codigo_remos' => 'Código de remos',
            'codigo_categoria' => 'Código categoria',
        ];
    }

    /**
     * Gets query for [[Fechasusos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFechasusos()
    {
        return $this->hasMany(Fechasuso::className(), ['id_uso' => 'id_uso']);
    }

    /**
     * Gets query for [[MatriculaBarco]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculaBarco()
    {
        return $this->hasOne(Barcos::className(), ['matricula' => 'matricula_barco']);
    }

    /**
     * Gets query for [[CodigoCategoria]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoCategoria()
    {
        return $this->hasOne(Categorias::className(), ['codigo' => 'codigo_categoria']);
    }

    /**
     * Gets query for [[CodigoRemos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoRemos()
    {
        return $this->hasOne(Remos::className(), ['codigo' => 'codigo_remos']);
    }
}
