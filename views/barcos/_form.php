<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Barcos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="barcos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'matricula')->textInput( ['placeholder' => 'Formato 999-AA', 'maxlength' => true]) ?>
    
    <?php 
        $tiposbarcos = [
        'Batel' => 'Batel', 
        'Trainerilla' => 'Trainerilla',
        'Trainera' => 'Trainera', 
        'Skiff 1x' => 'Skiff 1x',
        'Doble Scull 2x' => 'Doble Scull 2x', 
        'Dos Sin Timonel 2-' => 'Dos Sin Timonel 2-',
        'Dos Con Timonel 2+' => 'Dos Con Timonel 2+', 
        'Cuatro Scull 4x' => 'Cuatro Scull 4x',
        'Cuatro Sin Timonel 4-' => 'Cuatro Sin Timonel 4-', 
        'Cuatro Con Timonel 4+' => 'Cuatro Con Timonel 4+',
        'Ocho Con Timonel 8+' => 'Ocho Con Timonel 8+',
    ];
    
    echo $form->field($model, 'nombre_tecnico')->dropdownList($tiposbarcos,
        ['prompt'=>'Nombre técnico']
        );
    ?>

    <?= $form->field($model, 'mote')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fabricante')->textInput(['maxlength' => true]) ?>

    <div class="botonesadd">
        <?= Html::a('VOLVER A BARCOS', ['/barcos/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
