<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Barcos */

$this->title = 'Nuevo Barco';
$this->params['breadcrumbs'][] = ['label' => 'Barcos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="barcos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
