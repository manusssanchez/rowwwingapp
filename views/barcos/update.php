<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Barcos */

$this->title = 'Editar información de ' . $model->mote;
$this->params['breadcrumbs'][] = ['label' => 'Barcos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->mote, 'url' => ['view', 'id' => $model->matricula]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="barcos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
