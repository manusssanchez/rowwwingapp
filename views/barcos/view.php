<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Barcos */

$this->title = $model->mote;
$this->params['breadcrumbs'][] = ['label' => 'Barcos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="barcos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'matricula',
            'nombre_tecnico',
            'mote',
            'fabricante',
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A BARCOS', ['/barcos/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->matricula], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->matricula], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar este barco?',
                'method' => 'post',
            ],
        ]) ?>
        
    </p>
    

</div>
