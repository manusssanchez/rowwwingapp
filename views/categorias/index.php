<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categorias';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="cat-header">  
     
    <div class="col-lg-6">
        <?php 
            echo Html::a(
                    Html::img(
                            '@web/images/bg-remeros.jpg', 
                            ['class' => 'img-responsive img-border']
                    ),
                            ['remeros/index']
                ); 
        ?>
    </div>
    
    <div class="col-lg-6">
        <?php 
            echo Html::a(
                    Html::img(
                            '@web/images/bg-entrenadores.jpg', 
                            ['class' => 'img-responsive img-border']
                    ),
                            ['entrenadores/index']
                ); 
        ?>
    </div>
    
</div>

<div  class="indexmodel">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombre',
            'codigo',
            [
                'attribute' => 'Rango de edad',
                'value' => 'rango_edad'
            ],                       
            [
                'attribute' => 'Remeros',
                'value' => 'num_remeros'
            ],
            ['class' => 'yii\grid\ActionColumn',
                 'header' =>'Componentes',
                 'template' => '{remeros}',
                 'buttons' => [
                     'remeros' => function($url, $model) {
                        return Html::a('+info', ['categorias/remerosporcategoria', 'codigo' => $model->codigo], 
                                ['class' => 'btn btn-primary btn-md']);
                     },
                 ],
             ],
            
        ],
    ]); ?>
    
    <p class="botonesadd">
        <?= Html::a('AÑADIR REMERO', ['/remeros/create'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR ENTRENADOR', ['/entrenadores/create'], ['class' => 'btn btn-primary']) ?>
    </p>


</div>
