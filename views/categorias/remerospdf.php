<?php
    use yii\grid\GridView;
    
    $this->title = 'Remeros de la categoria '.$codigo;
    $this->params['breadcrumbs'][] = ['label' => 'Categorias', 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
 ?>

<div class="indexmodel">
    
    <h2>
        <?= $titulo_entrenadores ?>
    </h2>

    <?= GridView::widget([
        'dataProvider'=>$entrenadores,
        'columns'=>$campos_entrenadores,
    ]); ?>

    
    <h2>
        <?= $titulo_remeros ?>
    </h2>

    <?= GridView::widget([
        'dataProvider'=>$remeros,
        'columns'=>$campos_remeros,
    ]); ?>
   
</div>