<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Cuotas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cuotas-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php 
    
    $listdata=ArrayHelper::map($modelremeros,'id_remero','nombre_completo');?>
    
    
    <?= $form->field($model, 'id_remero')->dropDownList($listdata, ['prompt' => 'Nombre del remero'])->label('Nombre del remero')
    
    ?>
    
    
    <!-- MESES -->
    <?php 
    $meses = [
        'Enero' => 'Enero', 
        'Febrero' => 'Febrero',
        'Marzo' => 'Marzo', 
        'Abril' => 'Abril',
        'Mayo' => 'Mayo', 
        'Junio' => 'Junio',
        'Julio' => 'Julio', 
        'Agosto' => 'Agosto',
        'Septiembre' => 'Septiembre', 
        'Octubre' => 'Octubre',
        'Noviembre' => 'Noviembre',
        'Diciembre' => 'Diciembre'
    ];
    
    echo $form->field($model, 'mes')->dropdownList($meses,
        ['prompt'=>'Mes de la cuota']
        );
    ?>
    
    <?= $form->field($model, 'importe')->textInput(['type'=>'number']) ?>

    <?php 
        echo '<br>';
        echo '<label class="control-label">Fecha de pago</label>';
        echo DatePicker::widget([
            'model'=>$model,
            'attribute' => 'fecha_pago',
            'options' => ['placeholder' => 'Introduzca la fecha de pago'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' =>  'dd/mm/yyyy',
                'autoclose' => true,
            ]
        ]);
    ?>
    
    <br>
    
    <div class="botonesadd">
        <?= Html::a('VOLVER A CUOTAS', ['/cuotas/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
