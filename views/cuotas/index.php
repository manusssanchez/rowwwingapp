<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cuotas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Nombre del remero',
                'value' => 'remero.nombre_completo',
            ],
            'mes',
            'importe',
            'fecha_pago',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A TESORERIA', ['/site/tesoreria'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR CUOTA', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


</div>
