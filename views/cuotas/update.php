<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cuotas */

$this->title = 'Editar la cuota de ' . $model->remero['nombre_completo'];
$this->params['breadcrumbs'][] = ['label' => 'Cuotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->remero['nombre_completo'], 'url' => ['view', 'id' => $model->id_cuota]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="cuotas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelremeros' => $modelremeros,
    ]) ?>

</div>
