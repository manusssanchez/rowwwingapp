<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cuotas */

$this->title = $model->remero['nombre_completo'];
$this->params['breadcrumbs'][] = ['label' => 'Cuotas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="cuotas-view">

    <h1><?= Html::encode($this->title) ?></h1>
    

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Nombre completo',
                'value' => $model->remero['nombre_completo'],
            ],
            'mes',
            'importe',
            'fecha_pago',
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A CUOTAS', ['/cuotas/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->id_cuota], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->id_cuota], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar esta cuota?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
