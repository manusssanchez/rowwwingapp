<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Directivos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="directivos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true, 'placeholder' => '99999999-A'])->label('DNI') ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true]) ?>

    <?php
        $cargos = [
           'Presidente' => 'Presidente', 
           'Vicepresidente' => 'Vicepresidente',
           'Tesorero' => 'Tesorero', 
           'Secretario' => 'Secretario',
           'Vocal' => 'Vocal', 
           'Delegado' => 'Delegado',
       ];

       echo $form->field($model, 'cargo')->dropdownList($cargos,
           ['prompt'=>'Cargo']
           );
    ?>

    <div  class="botonesadd">
        <?= Html::a('VOLVER A DIRECTIVOS', ['/directivos/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
