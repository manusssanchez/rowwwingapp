<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Directivos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>

    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'dni',
            'nombre_completo',
            'cargo',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    
    <p class="botonesadd">
        <?= Html::a('AÑADIR DIRECTIVO', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
