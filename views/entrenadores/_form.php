<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entrenadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true,  'placeholder' => '99999999A'])->label('DNI') ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true]) ?>

    <?php 
    $niveles = [
        'Ninguna' => 'Ninguna',
        'Básico' => 'Básico',
        'Nivel I' => 'Nivel I',
        'Regional' => 'Regional', 
        'Avanzado' => 'Avanzado',
        'Nacional' => 'Nacional'
    ];
    
    echo $form->field($model, 'titulacion')->dropdownList($niveles,
        ['prompt'=>'Titulación']
        );
    ?>

    <?php
    $listdirectivos= ArrayHelper::map($modeldirectivos,'dni','nombre_completo'); ?>
    
    <?= $form->field($model, 'dni_directivo')->dropDownList($listdirectivos,['prompt' => 'Directivos'])->label('Nombre del directivo que lo contrata')?>
    

     <?php
    $listcategorias= ArrayHelper::map($modelcategorias,'codigo','nombre'); ?>
    
    <?= $form->field($model,'codigo_categoria')->dropDownList($listcategorias,['prompt' => 'Categorias'])->label('Categoría que entrenará') ?>

    <div class="botonesadd">
        <?= Html::a('VOLVER A ENTRENADORES', ['/entrenadores/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
