<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */

$this->title = 'Nuevo Entrenador';
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modeldirectivos' => $modeldirectivos,
        'modelcategorias' => $modelcategorias,
    ]) ?>

</div>
