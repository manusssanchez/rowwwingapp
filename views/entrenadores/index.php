<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entrenadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>

    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'dni',
            'nombre_completo',
            'titulacion',
            'dni_directivo',
            [
                'attribute' => 'Categoría que entrena',
                'value' => 'codigoCategoria.nombre'
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p class="botonesadd">
        <?= Html::a('VOLVER A CATEGORIAS', ['categorias/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR ENTRENADOR', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>
    

</div>
