<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Entrenadores */

$this->title = $model->nombre_completo;
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="entrenadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'dni',
            'nombre_completo',
            'titulacion',
            'dni_directivo',
            'codigo_categoria',
        ],
    ]) ?>

    <p class="botonesadd">
        <?= Html::a('VOLVER A ENTRENADORES', ['/entrenadores/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->dni], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->dni], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar este entrenador?',
                'method' => 'post',
            ],
        ]) ?>

    </p>
    
</div>
