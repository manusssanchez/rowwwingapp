<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Fechasuso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechasuso-form">
    
    <br>

    <?php $form = ActiveForm::begin(); ?>

    <?php
    $listadeusos = ArrayHelper::map($model_usos,'id_uso', function($model) {
                return 'Barco: '.$model->matriculaBarco['mote'].' - Remos: '.$model->codigoRemos['fabricante'].' - Categoría: '.$model->codigoCategoria['nombre'];
            });   
    ?>
    
    <?=
    $form->field($model,'id_uso')->dropDownList($listadeusos,
            [
                'prompt' => 'Escoge un uso',
    ])->label("Usos registrados");
    ?>
    
    <?php 
        echo '<br>';
        echo '<label class="control-label">Fecha de uso</label>';
        echo DatePicker::widget([
            'model'=>$model,
            'attribute' => 'fecha_uso',
            'options' => ['placeholder' => 'Introduzca la fecha de uso'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => true,
                'format' =>  'dd/mm/yyyy',
                'autoclose' => true,
            ]
        ]);
    ?>
    
    <br>
    
    <div class="botonesadd">
        <?= Html::a('VOLVER A USOS', ['usosmaterial/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
