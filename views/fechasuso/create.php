<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fechasuso */

$this->title = 'Nueva fecha de un uso de material';
$this->params['breadcrumbs'][] = ['label' => 'Fechasusos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechasuso-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_usos'=>$model_usos,
    ]) ?>

</div>
