<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fechas de uso';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Barco usado',
                'value' =>'uso.matriculaBarco.mote',
            ],
            [
                'attribute' => 'Remos usados',
                'value' =>'uso.codigoRemos.fabricante',
            ],
            [
                'attribute' => 'Categoría',
                'value' =>'uso.codigoCategoria.nombre',
            ],
            'fecha_uso',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    
    <p class="botonesadd">
        <?= Html::a('VOLVER A USOS', ['usosmaterial/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR FECHA', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
