<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Fechasuso */

$this->title = 'Editar fecha del uso del ' . $model->fecha_uso;
$this->params['breadcrumbs'][] = ['label' => 'Fechasusos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fecha_uso, 'url' => ['view', 'id' => $model->id_fecha]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="fechasuso-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'model_usos'=>$model_usos,
    ]) ?>

</div>
