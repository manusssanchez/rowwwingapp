<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Fechasuso */

$this->title = $model->id_fecha;
$this->params['breadcrumbs'][] = ['label' => 'Fecha de usos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?> 

<div class="fechasuso-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Barco utilizado',
                'value' => $model->uso['matricula_barco'],
            ],
            [
                'attribute' => 'Remos utilizados',
                'value' => $model->uso['codigo_remos'],
            ],
            [
                'attribute' => 'Categoría',
                'value' => $model->uso['codigo_categoria'],
            ],
            'fecha_uso',
        ],
    ]) ?>

    <p class="botonesadd">
        <?= Html::a('VOLVER A USOS', ['/usosmaterial/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->id_fecha], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->id_fecha], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar esta fecha?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    
</div>
