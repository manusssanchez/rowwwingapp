<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Gastos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="gastos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'asunto')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cantidad')->textInput(['type' => 'number']) ?>

    <?php 
        $listdirectivos= ArrayHelper::map($modeldirectivos,'dni','nombre_completo');
    ?>
    
    <?= $form->field($model,'dni_directivo')->dropDownList($listdirectivos,['prompt' => 'Directivo firmante'])->label('Directivos') ?>
   

    <div class="botonesadd">
        <?= Html::a('VOLVER A GASTOS',['gastos/index'],['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
