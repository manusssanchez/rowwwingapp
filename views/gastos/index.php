<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gastos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>

    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'asunto',
            'cantidad',
            [
                'attribute' => 'Directivo firmante',
                'value' => 'dniDirectivo.nombre_completo',
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A TESORERÍA', ['site/tesoreria'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR GASTO', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


</div>
