<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gastos */

$this->title = 'Editar gasto de ' . $model->asunto;
$this->params['breadcrumbs'][] = ['label' => 'Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asunto, 'url' => ['view', 'id' => $model->id_gasto]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="gastos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modeldirectivos' => $modeldirectivos,
    ]) ?>

</div>
