<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Gastos */

$this->title = $model->asunto;
$this->params['breadcrumbs'][] = ['label' => 'Gastos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="gastos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'asunto',
            'cantidad',
            [
                'attribute' => 'Directivo firmante',
                'value' => $model->dniDirectivo['nombre_completo'],
            ],
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A GASTOS',['gastos/index'],['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->id_gasto], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('BORRAR', ['delete', 'id' => $model->id_gasto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
