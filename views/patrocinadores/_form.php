<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinadores */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patrocinadores-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cantidad_aportada')->textInput(['type' => 'number']) ?>
    
    <?php 
        $listdirectivos= ArrayHelper::map($modeldirectivos,'dni','nombre_completo');
    ?>
    
    <?= $form->field($model, 'dni_directivo')->dropDownList($listdirectivos,['prompt' => 'Directivo que lo contrata'])->label('Nombre del directivo') ?>

    <?= $form->field($model, 'temporada_ini')->textInput(['maxlength' => true, 'type' => 'number'])->label('Temporada de inicio del contrato') ?>

    <?= $form->field($model, 'temporada_fin')->textInput(['maxlength' => true, 'type' => 'number'])->label('Temporada de fin del contrato') ?>

    <div class="botonesadd">
        <?= Html::a('VOLVER A PATROCINADORES', ['/patrocinadores/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
        
    </div>

    <?php ActiveForm::end(); ?>

</div>
