<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinadores */

$this->title = 'Nuevo Patrocinador';
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrocinadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modeldirectivos' => $modeldirectivos,
    ]) ?>

</div>
