<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Patrocinadores';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">
    
    <div class="cat-header">  
     
        <div class="col-lg-12">
            <?php 
                echo Html::a(
                        Html::img(
                                '@web/images/bg-patrocinios.jpg', 
                                ['class' => 'img-responsive img-border']
                        ),
                                ['patrocinios/index']
                    ); 
            ?>
        </div>
    
    </div>

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Patrocinador',
                'value' => 'nombre',
            ],
            'cantidad_aportada',
            'temporada_ini',
            'temporada_fin',
            [
                    'attribute' => 'Barcos patrocinados',
                    'value' => function($model) {
                               return join(', ', ArrayHelper::map($model->matriculaBarcos, 'matricula', 'mote'));
                    },
            ],
            [
                    'attribute' => 'Remero apadrinado',
                    'value' => function($model) {
                               return join(', ', ArrayHelper::map($model->remeros, 'codigo_patrocinador', 'nombre_completo'));
                    },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A TESORERÍA', ['site/tesoreria'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR PATROCINADOR', ['create'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR PATROCINIO', ['patrocinios/create'], ['class' => 'btn btn-primary']) ?>
    </p>



</div>
