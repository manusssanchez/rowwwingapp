<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinadores */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="patrocinadores-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'cantidad_aportada',
            [
                'attribute' => 'Directivo',
                'value' => $model->dniDirectivo['nombre_completo'],
            ],
            'temporada_ini',
            'temporada_fin',
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A PATROCINADORES', ['/patrocinadores/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar este patrocinador?',
                'method' => 'post',
            ],
        ]) ?>        
    </p>


</div>
