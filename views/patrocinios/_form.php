<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patrocinios-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php 
        $listpatrocinadores= ArrayHelper::map($modelpatrocinadores,'codigo','nombre');
    ?>
    
     <?= $form->field($model, 'codigo_patrocinador')->dropDownList($listpatrocinadores,['prompt' => 'Patrocinadores'])->label('Nombre del patrocinador') ?>

    <?php 
        $listbarcos= ArrayHelper::map($modelbarcos,'matricula','mote');
    ?>
    
     <?= $form->field($model, 'matricula_barco')->dropDownList($listbarcos,['prompt' => 'Barcos'])->label('Barco patrocinado') ?>


    <div class="botonesadd">
        <?= Html::a('VOLVER A PATROCINADORES', ['/patrocinadores/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
