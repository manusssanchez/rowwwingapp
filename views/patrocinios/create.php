<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinios */

$this->title = 'Nuevo Patrocinios';
$this->params['breadcrumbs'][] = ['label' => 'Patrocinios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrocinios-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelpatrocinadores' => $modelpatrocinadores,
        'modelbarcos' => $modelbarcos,
    ]) ?>

</div>
