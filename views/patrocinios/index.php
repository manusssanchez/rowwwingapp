<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Patrocinios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'attribute' => 'Patrocinador',
                'value' => 'codigoPatrocinador.nombre',
            ],
            [
                'attribute' => 'Barco patrocinado',
                'value' => 'matriculaBarco.mote',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A PATROCINADORES', ['patrocinadores/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR PATROCINIO', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>


</div>
