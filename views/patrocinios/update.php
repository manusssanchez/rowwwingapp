<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinios */

$this->title = 'Editar patrocinio de ' . $model->codigoPatrocinador['nombre'];
$this->params['breadcrumbs'][] = ['label' => 'Patrocinios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigoPatrocinador['nombre'], 'url' => ['view', 'id' => $model->id_patrocinio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patrocinios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelpatrocinadores' => $modelpatrocinadores,
        'modelbarcos' => $modelbarcos,
    ]) ?>

</div>
