<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Patrocinios */

$this->title = $model->codigoPatrocinador['nombre'];
$this->params['breadcrumbs'][] = ['label' => 'Patrocinios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="patrocinios-view">

    <h1><?= Html::encode($this->title) ?></h1>

   

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Patrocinador',
                'value' => $model->codigoPatrocinador['nombre'],
            ],
            [
                'attribute' => 'Barco patrocinado',
                'value' => $model->matriculaBarco['mote'],
            ],
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A PATROCINADORES', ['/patrocinadores/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->id_patrocinio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->id_patrocinio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro quieres eliminar este patrocinio?',
                'method' => 'post',
            ],
        ]) ?>
        
    </p>

</div>
