<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Remeros */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remeros-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        
        $listcategorias= ArrayHelper::map($modelcategorias,'codigo','nombre');
   
        echo $form->field($model, 'codigo_categoria')
                ->dropDownList($listcategorias,['prompt' => 'Categorías'])
                ->label('Categoría que pertenece');
        
    ?>

    <?= $form->field($model, 'codigo_remero')->textInput(['type' => 'number'])->label('Número de licencia') ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true])->label('DNI') ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true]) ?>

     <?php 
        echo '<br>';
        echo '<label class="control-label">Fecha de nacimiento</label>';
        echo DatePicker::widget([
            'model'=>$model,
            'attribute' => 'fecha_nac',
            'value' => '1-Ene-2000',
            'options' => ['placeholder' => 'Introduzca la fecha de nacimiento'],
            'pluginOptions' => [
                'todayHighlight' => true,
                'todayBtn' => false,
                'format' =>  'dd/mm/yyyy',
                'autoclose' => true,
            ]
        ]);
    ?>
    
    <br>
    
    <?php 
        
        $listpatrocinadores= ArrayHelper::map($modelpatrocinadores,'codigo','nombre');
   
        echo $form->field($model, 'codigo_patrocinador')->dropDownList($listpatrocinadores,['prompt' => 'Ninguno'])->label('Padrino');
    ?>

    <?= $form->field($model, 'lesiones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'anios_exp')->textInput(['type' => 'number'])->label('Años de experiencia') ?>

    <?= $form->field($model, 'dni_padre')->textInput(['maxlength' => true])->label('DNI progenitor/tutor legal') ?>

    <?= $form->field($model, 'nombre_padre')->textInput(['maxlength' => true])->label('Nombre progenitor/tutor legal') ?>

    <div class="botonesadd">
        <?= Html::a('VOLVER A REMEROS', ['/remeros/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
