<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Remeros */

$this->title = 'Nuevo Remero';
$this->params['breadcrumbs'][] = ['label' => 'Remeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remeros-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelcategorias' => $modelcategorias,
        'modelpatrocinadores' => $modelpatrocinadores,
    ]) ?>

</div>
