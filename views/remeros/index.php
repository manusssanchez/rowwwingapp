<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Remeros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nombre_completo',
            [
                'attribute' => 'Categoria',
                'value' => 'codigo_categoria',
            ], 
            'codigo_remero',
            'dni',
            //'fecha_nac',
            //'codigo_patrocinador',
            //'lesiones',
            //'anios_exp',
            //'dni_padre',
            //'nombre_padre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p class="botonesadd">
        <?= Html::a('VOLVER A CATEGORIAS', ['/categorias/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR REMERO', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
