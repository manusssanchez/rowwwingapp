<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Remeros */

$this->title = 'Cambiar datos de ' . $model->nombre_completo;
$this->params['breadcrumbs'][] = ['label' => 'Remeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre_completo, 'url' => ['view', 'id' => $model->codigo_remero]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="remeros-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelcategorias' => $modelcategorias,
        'modelpatrocinadores' => $modelpatrocinadores,  
    ]) ?>

</div>
