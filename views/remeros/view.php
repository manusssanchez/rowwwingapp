<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Remeros */

$this->title = $model->nombre_completo;
$this->params['breadcrumbs'][] = ['label' => 'Remeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="remeros-view">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Categoría',
                'value' => $model->codigoCategoria['nombre'],
            ],
            [
                'attribute' => 'Numero de licencia',
                'value' => $model->codigo_remero,
            ],
            'dni',
            'nombre_completo',
            'fecha_nac',
            [
                'attribute' => 'Padrino',
                'value' => $model->codigoPatrocinador['nombre'],
            ],
            'lesiones',
            [
                'attribute' => 'Años experiencia',
                'value' => $model->anios_exp,
            ],
            'dni_padre',
            'nombre_padre',
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A REMEROS', ['/remeros/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->id_remero], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->id_remero], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar este remero?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
