<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Remos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'fabricante')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'num_remos')->textInput(['type'=>'number']) ?>
    
    <?php 
    $dureza = [
        'Muy baja' => 'Muy baja', 
        'Baja' => 'Baja',
        'Media' => 'Media', 
        'Moderada' => 'Moderada',
        'Alta' => 'Alta',
        'Muy alta' => 'Muy alta'
    ];
    
    echo $form->field($model, 'dureza')->dropdownList($dureza,
        ['prompt'=>'Dureza']
        );
    ?>

    <div class="botonesadd">
        <?= Html::a('VOLVER A REMOS', ['remos/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
