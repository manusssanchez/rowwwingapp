<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Remos */

$this->title = 'Nuevos Remos';
$this->params['breadcrumbs'][] = ['label' => 'Remos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
