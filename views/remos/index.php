<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Remos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>
    
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'codigo',
            'fabricante',
            'num_remos',
            'dureza',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p class="botonesadd">
        <?= Html::a('VOLVER A MATERIAL', ['usosmaterial/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR REMOS', ['create'], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
