<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Remos */

$this->title = 'Editar remos de ' . $model->fabricante;
$this->params['breadcrumbs'][] = ['label' => 'Remos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->fabricante, 'url' => ['view', 'id' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="remos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
