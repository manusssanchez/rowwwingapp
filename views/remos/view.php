<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Remos */

$this->title = $model->fabricante;
$this->params['breadcrumbs'][] = ['label' => 'Remos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="remos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'codigo',
            'fabricante',
            'num_remos',
            'dureza',
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A REMOS', ['remos/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->codigo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->codigo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar estos remos?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
