<?php

/* @var $this yii\web\View */

$this->title = 'RowwwningApp';
?>
<div class="site-index">

    <div class="inicio">
        <img src="../web/images/rowing-title.png" width="100%">
    </div>

    <div class="body-content">
        
       <div class="row1">
            <div class="col-lg-8">
               <img src="../web/images/ini_pic.jpg" width="100%">
            </div>
            
            <div class="col-lg-4">
                
                <div class="remeros">
                    <a href="remeros/index" class="urlindex">
                        <p class="masinfo">+info</p>
                        <p class="datoremo"><?= $numremeros ?></p>
                        <h3>Remeros</h3>
                    </a>
                </div>
                
                <br>
                
                <div class="entrenadores">
                    <a href="entrenadores/index" class="urlindex">
                        <p class="masinfo">+info</p>
                        <p class="datoremo"><?= $numentrenadores ?></p>
                        <h3>Entrenadores</h3>
                    </a>
                </div>
                
                <br>
                
                <div class="patrocinadores">
                    <a href="patrocinadores/index" class="urlindex">
                        <p class="masinfo">+info</p>
                        <p class="datoremo"><?= $numpatrocinadores ?></p>
                        <h3>Patrocinadores</h3>
                    </a>                
                </div>
                
            </div>
           
 
        </div>
        
        <br>
        
        
        <div class="row2">
            
            <div class="col-lg-6">
               <div class="barcos">
                    <a href="barcos/index" class="urlindex">
                        <p class="masinfo">+info</p>
                        <p class="datoremo"><?= $numbarcos ?></p>
                        <h3>Barcos</h3>
                    </a>
                </div>   
            </div>
            
            <div class="col-lg-6">
               <div class="remos">
                    <a href="remos/index" class="urlindex">
                        <p class="masinfo">+info</p>
                        <p class="datoremo"><?= $numremos ?></p>
                        <h3>Remos</h3>
                    </a>
                </div>
            </div>
            
            
        </div>

    </div>
</div>
