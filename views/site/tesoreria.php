<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tesoreria';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row-tes">  

        <div class="col-lg-8">
              <?php 
                    echo Html::a(
                            Html::img(
                                    '@web/images/bg-cuotas-tes.jpg', ['class' => 'img-responsive img-border']
                            ),
                                    ['cuotas/index']
                        ); 
                ?>
        </div>
        
        
        <div class="col-lg-4 tes-texto">
            <p class="datotes"><?=$cantcuotas?>€</p>
            <h3>Ingresos en cuotas</h3>
        </div>
 
    </div>
    
    
    <div class="row-tes">  
        
        <div class="col-lg-8">
              <?php 
                    echo Html::a(
                            Html::img(
                                    '@web/images/bg-patrocinadores-tes.jpg', 
                                    ['class' => 'img-responsive img-border',
                                      'position' => 'top']
                            ),
                                    ['patrocinadores/index']
                        ); 
                ?>
        </div>
        
        <div class="col-lg-4 tes-texto">
            <p class="datotes"><?=$cantpatrocinadores?>€</p>
            <h3>Aportaciones de patrocinadores</h3>
        </div>
        
    </div>
    
    
    <div class="row-tes">  
        
        
        <div class="col-lg-8">
              <?php 
                    echo Html::a(
                            Html::img(
                                    '@web/images/bg-gastos-tes.jpg', 
                                    ['class' => 'img-responsive img-border']
                            ),
                                    ['gastos/index']
                        ); 
                ?>
        </div>
        
        <div class="col-lg-4 tes-texto">
            <p class="datogasto"><?=$cantgastos?>€</p>
            <h3>Gastos</h3>
        </div>
        
    </div>

    

</div>
