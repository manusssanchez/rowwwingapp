<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Usosmaterial */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usosmaterial-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        $listbarcos= ArrayHelper::map($modelbarcos,'matricula','mote');
    ?>
    
    <?= $form->field($model, 'matricula_barco')->dropDownList($listbarcos,['prompt' => 'Barcos'])->label('Barco utilizado') ?>
    
    <?php
    $listaremos = ArrayHelper::map($modelremos,'codigo',function($model) {
                return 'Fabricante: '.$model->fabricante.' - Dureza: '.$model->dureza;
            });   
    ?>
    
    <?= $form->field($model,'codigo_remos')->dropDownList($listaremos,['prompt' => 'Remos'])->label('Remos utilizados') ?>

    
    <?php
        $listcategorias= ArrayHelper::map($modelcategorias,'codigo','nombre');
   
        echo $form->field($model, 'codigo_categoria')->dropDownList($listcategorias,['prompt' => 'Categorías'])->label('Categoría');
    ?>

    <div class="botonesadd">
        <?= Html::a('VOLVER A USOS', ['usosmaterial/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::submitButton('GUARDAR', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
