<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usosmaterial */

$this->title = 'Nuevo uso de material';
$this->params['breadcrumbs'][] = ['label' => 'Usosmaterials', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usosmaterial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelcategorias' => $modelcategorias,
        'modelbarcos' => $modelbarcos,
        'modelremos' => $modelremos,
    ]) ?>

</div>
