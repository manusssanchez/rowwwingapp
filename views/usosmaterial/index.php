<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registro de usos de material';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="mat-header">  
     
    <div class="col-lg-6">
        <?php 
            echo Html::a(
                    Html::img(
                            '@web/images/bg-barcos.jpg', 
                            ['class' => 'img-responsive img-border']
                    ),
                            ['barcos/index']
                ); 
        ?>
    </div>
    
    <div class="col-lg-6">
        <?php 
            echo Html::a(
                    Html::img(
                            '@web/images/bg-remos.jpg', 
                            ['class' => 'img-responsive img-border']
                    ),
                            ['remos/index']
                ); 
        ?>
    </div>
    
</div>

<div class="indexmodel">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?=GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
                [
                  'attribute' => 'Barco',
                    'value' => 'matriculaBarco.mote',
                ],
                [
                  'attribute' => 'Marca',
                    'value' => 'codigoRemos.fabricante',
                ],
                [
                  'attribute' => 'Dureza',
                    'value' => 'codigoRemos.dureza',
                ],
                [
                    'attribute' => 'Categoría',
                    'value' => 'codigoCategoria.nombre',
                ],
                [
                    'attribute' => 'Fecha de uso',
                    'value' => function($model) {
                               return join(', ', ArrayHelper::map($model->fechasusos, 'fecha_uso', 'fecha_uso'));
                    },
                ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p class="botonesadd">
        <?= Html::a('AÑADIR NUEVO USO', ['create'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('AÑADIR NUEVA FECHA', ['/fechasuso/create'], ['class' => 'btn btn-primary']) ?>
    </p>

</div>
