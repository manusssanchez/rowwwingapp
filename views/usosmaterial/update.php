<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Usosmaterial */

$this->title = 'Uso del barco '.$model->matriculaBarco['mote'].' y remos '.$model->codigoRemos['fabricante'];
$this->params['breadcrumbs'][] = ['label' => 'Usos de Materiales', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Barco: '.$model->matriculaBarco['mote'].' - Remos: '.$model->codigoRemos['fabricante'], 'url' => ['view', 'id' => $model->id_uso]];
$this->params['breadcrumbs'][] = 'Editar';
?>
<div class="usosmaterial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'modelcategorias' => $modelcategorias,
        'modelbarcos' => $modelbarcos,
        'modelremos' => $modelremos,
    ]) ?>

</div>
