<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Usosmaterial */

$this->title = 'Uso del barco '.$model->matriculaBarco['mote'].' y remos '.$model->codigoRemos['fabricante'];
$this->params['breadcrumbs'][] = ['label' => 'Usos de material', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usosmaterial-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'Barco utilizado',
                'value' => $model->matriculaBarco['mote'],
            ],
            [
                'attribute' => 'Remos utilizados',
                'value' => $model->codigoRemos['fabricante'],
            ],
            [
                'attribute' => 'Categoría que lo ha utilizado',
                'value' => $model->codigoCategoria['nombre'],
            ],
            [
                'attribute' => 'Días que se ha usado',
                'value' => function($model) {
                           return join(', ', ArrayHelper::map($model->fechasusos,'fecha_uso','fecha_uso'));
                },
            ],
        ],
    ]) ?>
    
    <p class="botonesadd">
        <?= Html::a('VOLVER A USOS', ['usosmaterial/index'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('EDITAR', ['update', 'id' => $model->id_uso], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('ELIMINAR', ['delete', 'id' => $model->id_uso], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Seguro que quieres eliminar este uso?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

</div>
